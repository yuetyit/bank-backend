const express = require("express");
const app = express();

const jwt = require("jsonwebtoken");
const jwtSecret = "server";
const { createHash } = require("crypto");

const db=[];

app.use(express.static('public'));
app.use(express.json());

function getJWT(data) {
  return jwt.sign(
      data,
      jwtSecret
  );
}

function getHash(data) {
  const hash = createHash("sha256");

  hash.update(data);
  return hash.digest("hex");
}

app.post("/member/register", (req, res) => {
  const id = req.body.id;
  const password = req.body.password;
  const name = req.body.name;

  if (!id || !password || !name) {

    return res.json({
      success: false,
      message: "Insufficient info",
    });
  }

  if(db.find(member=> member.id === id)){
    return res.json({
        success: false,
        message: "Registered member",
    });
  }

  db.push({
    id: id,
    password: getHash(id + password),
    name: name,
    balance:0
  });

  return res.json({
    success: true,
    token: getJWT({id,name})
  });

});

app.post("/member/login", (req, res) => {
  const id = req.body.id;
  const password = req.body.password;

  if (!id || !password ) {
      return res.json({
        success: false,
        message: "Insufficient info",
      });
  }

  const found = db.find((member)=> member.id === id);
  
  if(!found){
      return res.json({
          success: false,
          message: "Wrong id or password",
      });
  }

  if(found.password === getHash(id+password)){
      return res.json({
          success: true,
          token:getJWT({id,name:found.name}),
      });
  }else{
      return res.json({
          success: false,
          message: "Wrong id or password",
      });
  }
})

function auth(req, res, next) {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, jwtSecret, (err, decoded) => {
      if (err) {
        res.status(401).json({
          success: false,
        });
      } else {
        req.userInfo = decoded;
        next();
      }
    });
  } else {
    return res.status(401).json({
      success: false,
    });
  }
}

app.post("/member/transfer", auth, (req,res)=>{
  const receiver = req.body.to;
  let money = req.body.money;
  const myId = req.user.id;

  if(!isNaN(money)){
    money = Number(money);
  }else{
    return res.json({success:false, message:"wrong data format; money should be a number"});
  }
  
  const found = db.find((member)=> member.id === receiver);
  const me = db.find((member)=> member.id === myId);
  if(found  && me && money>0 && me.balance >= money){
    found.balance+=money;
    me.balance-=money;  
    return res.json({success:true});  
  }

  return res.json({success:false, message:"No such user / wrong money format"});  
 
})

app.post("/member/topup", auth, (req,res)=>{
  let money = req.body.money;
  const myId = req.user.id;

  if(!isNaN(money)){
    money = Number(money);
  }else{
    return res.json({success:false, message:"wrong data format; money should be a number"});
  }
  
  const found = db.find((member)=> member.id === myId);
  if(found && money>0 ){
    found.balance+=money;    
    return res.json({success:true});  
  }

  return res.json({success:false, message:"No such user / wrong money format"});  
 
})

app.post("/member/profile", auth, (req, res)=>{
  const myId = req.user.id;
  
  const found = db.find((member)=> member.id === myId);
  const userObj = { ...found };
  if(found){
    delete userObj.password;
    return res.json({
      success:true,
      ...userObj
    })
  }

  return res.json({
    success:false,
    message:"No such user"
  })
  
  
})

let port = 3011;
app.listen(port, () => {
  console.log(`Web server is running on port ${port}!`);
});
